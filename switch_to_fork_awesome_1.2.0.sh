#!/bin/sh

# SPDX-FileCopyrightText: 2022 tytan652
#
# SPDX-License-Identifier: WTFPL

fa_version='1.2.0'
fa_sha256sum='23fba5f191f204e0414c547bf4c9b10fd7ca42c151260e8f64698449a75fbdb3'

if [[ $# -ne 1 ]]; then
    echo 'Too many/no arguments, expecting LoveIt directory' >&2
    exit 1
fi

if [[ ! -d "$1" ]]; then
   echo 'The argument is not a directory' >&2
   exit 1
fi

if ! command -v curl &> /dev/null
then
    echo 'curl is not installed' >&2
    exit 1
fi

if ! command -v tar &> /dev/null
then
    echo 'tar is not installed' >&2
    exit 1
fi

echo 'Removing Font Awesome files'
rm -rf "$1/assets/lib/fontawesome-free"
rm -rf "$1/static/lib/webfonts"
echo -e 'Files removed\n'


if [[ ! -f "$fa_version.tar.gz" ]]; then
    echo "Downloading Fork Awesome $fa_version tarball"
    curl -O -L https://github.com/ForkAwesome/Fork-Awesome/archive/${fa_version}.tar.gz
    echo -e 'Tarball downloaded\n'
fi

echo  'Checking Fork Awesome tarball checksum'
if ! echo "$fa_sha256sum ${fa_version}.tar.gz" | sha256sum -c -; then
    echo 'Checksum failed' >&2
    exit 1
fi
echo -e 'Checksum passed\n'

echo  'Extracting Fork Awesome tarball'
tar -xzf "${fa_version}.tar.gz"
echo -e 'Tarball extracted\n'

if [[ ! -d "Fork-Awesome-$fa_version" ]]; then
   echo "No 'Fork-Awesome-$fa_version' directory found" >&2
   exit 1
fi

echo 'Copying Fork Awesome files to LoveIt theme'
mkdir -p "$1/assets/lib/forkawesome"
cp Fork-Awesome-$fa_version/css/fork-awesome.min.css $1/assets/lib/forkawesome/
cp Fork-Awesome-$fa_version/css/v5-compat.min.css $1/assets/lib/forkawesome/
cp Fork-Awesome-$fa_version/fonts/* $1/static/lib/fonts/
rm -rf "Fork-Awesome-$fa_version"
echo -e 'Files copied\n'

echo 'Add Fork Awesome to LoveIt jsdelivr config'
echo "  # forkawesome@$fa_version https://forkaweso.me/Fork-Awesome/" >> $1/assets/data/cdn/jsdelivr.yml
echo "  forkawesomeCSS: fork-awesome@$fa_version/css/fork-awesome.min.css" >> $1/assets/data/cdn/jsdelivr.yml
echo "  forkawesomeCompatCSS: fork-awesome@$fa_version/css/v5-compat.min.css" >> $1/assets/data/cdn/jsdelivr.yml
echo -e 'Done\n'

echo 'Replace Font Awesome CSS by Fork Awesome CSS'
sed -i 's/Font Awesome/Fork Awesome/g' $1/layouts/partials/head/link.html
sed -i 's/fontawesomeFreeCSS/forkawesomeCSS/g' $1/layouts/partials/head/link.html
sed -i 's|lib/fontawesome-free/all.min.css|lib/forkawesome/fork-awesome.min.css|g' $1/layouts/partials/head/link.html
echo -e '\n{{- /* Fork Awesome V5 Compat */ -}}' >> $1/layouts/partials/head/link.html
echo '{{- $source := $cdn.forkawesomeCompatCSS | default "lib/forkawesome/v5-compat.min.css" -}}' >> $1/layouts/partials/head/link.html
echo '{{- $style := dict "Source" $source "Fingerprint" $fingerprint -}}' >> $1/layouts/partials/head/link.html
echo '{{- partial "plugin/style.html" $style -}}' >> $1/layouts/partials/head/link.html
echo -e 'Done\n'

echo 'Remove Font Awesome algolia icon'
sed -i 's|<i class=\\"fab fa-algolia fa-fw\\"><\/i>||g' $1/assets/js/theme.min.js
sed -i 's|<i class=\\"fab fa-algolia fa-fw\\"><\/i>||g' $1/assets/js/theme.min.js.map
echo -e 'Done\n'

echo 'Fix Telegram and Dev.to social icons'
sed -i 's/fa-telegram-plane/fa-telegram/g' $1/assets/data/social.yml
sed -i 's/fa-dev /fa-dev-to /g' $1/assets/data/social.yml
echo -e 'Done\n'

echo 'Modernize YouTube social icons'
sed -i 's/fa-youtube/fa-youtube-play/g' $1/assets/data/social.yml
echo -e 'Done\n'

echo 'Replace unavailable social icons with Simpleicons'
sed -i 's/"Class" "fab fa-viber fa-fw"/"Simpleicons" "viber" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-buffer fa-fw"/"Simpleicons" "buffer" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-flipboard fa-fw"/"Simpleicons" "flipboard" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-blogger fa-fw"/"Simpleicons" "blogger" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-evernote fa-fw"/"Simpleicons" "evernote" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/Class: fab fa-mix fa-fw/Simpleicons: mix/g' $1/assets/data/social.yml
sed -i 's/"Class" "fab fa-mix fa-fw"/"Simpleicons" "mix" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-goodreads fa-fw"/"Simpleicons" "goodreads" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-kickstarter fa-fw"/"Simpleicons" "kickstarter" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
sed -i 's/"Class" "fab fa-strava fa-fw"/"Simpleicons" "strava" "Prefix" \(\.Scratch.Get "cdn" \| default dict\)\.simpleIconsPrefix/g' $1/layouts/partials/plugin/share.html
echo -e 'Done\n'

echo 'Replace Simpleicons social icons with Fork Awesome'
sed -i 's/Simpleicons: gitea/Class: fab fa-gitea fa-fw/g' $1/assets/data/social.yml
sed -i 's/Simpleicons: xmpp/Class: fab fa-xmpp fa-fw/g' $1/assets/data/social.yml
sed -i 's/Simpleicons: matrix/Class: fab fa-matrix-org fa-fw/g' $1/assets/data/social.yml
echo -e 'Done\n'

echo 'Replace Font Awesome icons that are not compatble with Fork Awesome'
echo '  - Replace "fa-kiss-wink-heart" with "heart-o" in footer.html'
sed -i 's/fa-kiss-wink-heart/fa-heart-o/g' $1/layouts/partials/footer.html
echo '  - Replace "fa-calendar-alt" with "fa-calendar" in single.html'
sed -i 's/fa-calendar-alt/fa-calendar/g' $1/layouts/posts/single.html
echo '  - Replace "fa-lightbulb" with "fa-lightbulb-o" in admonition.html'
sed -i 's/fa-lightbulb/fa-lightbulb-o/g' $1/layouts/shortcodes/admonition.html
echo '  - Replace "fa-skull-crossbones" with "fa-exclamation-circle" in admonition.html'
sed -i 's/fa-skull-crossbones/fa-exclamation-circle/g' $1/layouts/shortcodes/admonition.html
echo -e 'Done\n'

echo 'LoveIt now use Fork Awesome'