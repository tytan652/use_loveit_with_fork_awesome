<!--
SPDX-FileCopyrightText: 2022 tytan652

SPDX-License-Identifier: WTFPL
-->

# Use [LoveIt][1] with [Fork Awesome][2]

**NOTE: Since [LoveIt][1] has upgraded Font Awesome 6, I recommend for now to stick with [LoveIt][1] v0.2.10 for now.**

Just a script that modify the Hugo theme [LoveIt][1] to use [Fork Awesome][2] rather than Font Awesome.

Requires cURL, GNU Tar and sed.

Usage:
```sh
./switch_to_fork_awesome_1.2.0.sh ../themes/LoveIt
```

What does the script do ?:
1. Remove Font Awesome file from the theme
2. Download Fork Awesome tarball (skipped if already downloaded)
3. Check the tarball checksum
4. Extract the tarball
5. Copy CSS and fonts in the theme
6. Add Fork Awesome to the jsdelivr config
7. Replace Font Awesome CSS call by Fork Awesome
8. Remove algolia icon usage since Fork Awesome does not have it
9. Fix Telegram and Dev.to icon
10. Modernize YouTube social icons , Fork Awesome have a old YT icon and a play button version 
11. Replace unavailable social icons by [Simpleicons][3] one
12. Replace Simpleicons social with Fork Awesome one
13. Replace Font Awesome icon that are not compatible with Fork Awesome

[1]: https://hugoloveit.com/
[2]: https://forkaweso.me/Fork-Awesome/
[3]: https://simpleicons.org/
